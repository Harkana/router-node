let App = require('./Router.js')

let app = App.run()						    
								    
app.get('/article', (req, resp) => {			    
    resp.write('hello article');				    
    resp.end()
})								    

app.post('/test', (req,resp) => {
    resp.end()
})

app.get('/article/:id', (params, req, resp) => {		    
    resp.write('hello article '+ params["id"] + '!')		    
    resp.end()
})								    
								    
app.get('/', (req, resp) => {				    
    resp.views('index.html', {})
})								    
								    								    
app.get('/message/:id/test',(params, req, resp) => {	    
    resp.write('test 11 marche')				    
    for (let key in params){					   
    	resp.write(key + '=>' + params[key])			    
    }							    
    resp.end()
})								    

app.get('/truc', function(req, resp){			    
    resp.write('i\'m in truc\n')				    
    resp.end()
})								    
								    
app.fallback((req,resp) => {					    
    resp.write('wesh !')					    
    resp.end()
})
								    
app.debug()							    
								    
App.listen(8888)						    

