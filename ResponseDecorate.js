const VIEWS = "views/"

class ResponseDecorate
{
    constructor()
    {
	this.res = {}
    }

    decorate(res)
    {
	res.views = this.views
    }
    
    views(file, params, ft)
    {
	const fs = require('fs')
	
	fs.readFile(VIEWS + file, 'utf-8', (err, data) => {
	    if (err) throw err

	    this.write(data)
	    this.end()
	})	
    }
}

module.exports = ResponseDecorate
