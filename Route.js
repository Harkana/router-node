class Route {
    
    constructor(a){
	this.params = {}
	this.requestUrl = a
	this.isEqual = false
	this.hasParams = false	
    }

    patternMatch(query){
	let request = this.requestUrl
	let nameParam = ""
	let valueParam = ""	
	let tmpParams = {}
	let i = 0
	let j = 0

	for (i = 0;i < query.length;i++)
	{	    
	    if (query[i] != request[j] && query[i] != ":"){
		tmpParams = {}
		return (false)
	    }
	    else if (query[i] == ":"){
		while (query[i] && query[i] == ":"){
		    i++
		}
		while (query[i] && query[i] != "/"){
		    nameParam += query[i]
		    i++
		}
		while (request[j] && request[j] != "/"){
		    valueParam += request[j]
		    j++
		}
		tmpParams[nameParam] = valueParam
		nameParam = ""
		valueParam = ""
	    }
	    j++
	}
	if (query[i] != request[j]){
	    return (false)
	    tmpParams = {}
	}
	for (let key in tmpParams){
	    this.params[key] = tmpParams[key]
	    this.hasParams = true
	}
	this.isEqual = true
	return (true)
    }

    getParams(){
	return (this.params)
    }

    getHasParam(){
	return (this.hasParams)
    }

    getIsEqual(){
	return (this.isEqual)
    }
}

module.exports = Route
