'use strict'

const EventEmitter = require('events')
const ResponseDecorate = require('./ResponseDecorate')
const Route = require('./Route')
const PUBLIC  = "public"
const EXTENSIONS_AUTHORIZE = {
    'css' : {
	'Content-Type' : 'text/css',
    },
    'js' : {
	'Content-Type' : 'text/javascript'
    }
}

let App = {
    emitter : new EventEmitter(),
    server : null,
    urls : [],
    run : () => {
	App.server = require('http').createServer()
	App.server.on('request', (req, resp) => {
	    let flag = false
	    for (let key in EXTENSIONS_AUTHORIZE){
		if (req.url.endsWith(key)){
	    	    fs.readFile(PUBLIC + req.url, (err, data) => {
			if (err){
			    resp.writeHead(404)
			}
			resp.writeHead(200, EXTENSIONS_AUTHORIZE[key])
	    		resp.write(data)
	    		resp.end()
	    	    })
		    return
		}
	    }
	    for (let item of App.urls){
		let route = new Route(req.url)	
		if (route.patternMatch(item)){
		    new ResponseDecorate().decorate(resp)
		    if (route.getHasParam()){
			App.emitter.emit(req.method + item, route.getParams(), req, resp)
		    }
		    else{
			App.emitter.emit(req.method + item, req, resp)
		    }
		    flag = true
		}
	    }
	    if (!flag){
		App.emitter.emit('fallback', req, resp)
	    }	    
	})
	return ({
	    get : (url, ft) => {
		App.urls.push(url);
		App.emitter.on("GET" + url, ft)		   
	    },
	    post : (url, ft) => {
		App.urls.push(url);
		App.emitter.on("POST" + url, ft)		   
	    },
	    put : (url, ft) => {
		App.urls.push(url);
		App.emitter.on("PUT" + url, ft)		   
	    },
	    delete : (url, ft) => {
		App.urls.push(url);
		App.emitter.on("DELETE" + url, ft)		   
	    },
	    debug : () => {
		console.log(App.urls);
	    },
	    fallback : (ft) => {
		App.emitter.on('fallback', ft)
	    }
	})
    },
    listen : (port) => {
	App.port = port
	App.server.listen(port)
    }
}

module.exports = App
